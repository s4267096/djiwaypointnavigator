package com.dji.simulatorDemo;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import dji.common.error.DJISDKError;
import dji.common.flightcontroller.LocationCoordinate3D;
import dji.common.flightcontroller.simulator.InitializationData;
import dji.common.flightcontroller.simulator.SimulatorState;
import dji.common.flightcontroller.virtualstick.FlightControlData;
import dji.common.flightcontroller.virtualstick.FlightCoordinateSystem;
import dji.common.flightcontroller.virtualstick.RollPitchControlMode;
import dji.common.flightcontroller.virtualstick.VerticalControlMode;
import dji.common.flightcontroller.virtualstick.YawControlMode;
import dji.common.flightcontroller.virtualstick.Limits;
import dji.common.model.LocationCoordinate2D;
import dji.common.useraccount.UserAccountState;
import dji.common.util.CommonCallbacks;
import dji.log.DJILog;
import dji.sdk.base.BaseProduct;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.products.Aircraft;
import dji.common.error.DJIError;
import dji.sdk.sdkmanager.DJISDKManager;
import dji.sdk.useraccount.UserAccountManager;

import static java.lang.Math.abs;
import static java.lang.Math.atan2;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class MainActivity extends Activity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getName();

    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
    };
    private List<String> missingPermission = new ArrayList<>();
    private AtomicBoolean isRegistrationInProgress = new AtomicBoolean(false);
    private static final int REQUEST_PERMISSION_CODE = 12345;
    private static final double LAT_UNIT = 88516.0;
    private static final double LON_UNIT = 110986.0;
    private FlightController mFlightController;
    protected TextView mConnectStatusTextView;
    private Button mBtnEnableVirtualStick;
    private Button mBtnDisableVirtualStick;
    private ToggleButton mBtnSimulator;
    private Button mBtnTakeOff;
    private Button mBtnLand;

    private TextView mTextView;
    private TextView mLocTextView;
    private TextView mTangE;

    private Timer mSendVirtualStickDataTimer;
    private SendVirtualStickDataTask mSendVirtualStickDataTask;

    private Timer mCheckDriftTimer;
    private Timer mCheckTangentDriftTimer;
    private Timer mUpdateOutputTimer;
    private CheckDriftTimerTask mCheckDriftTimerTask;
    private CheckTangentDriftTimerTask mCheckTangentDriftTimerTask;
    private updateOutputTimerTask mUpdateOutputTimerTask;
    private double de_tang;
    private double e_tang;
    private double ie_tang;
    private double de_norm;
    private double e_norm;
    private double ie_norm;
    private double desiredWaypointX;
    private double desiredWaypointY;
    private double normalX;
    private double normalY;
    private double tangX;
    private double tangY;
    private double magTang;
    private double gKp = 1.0;
    private double gKd = 5.0;

    private float mPitch;
    private float mRoll;
    private float mYaw;
    private float mThrottle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAndRequestPermissions();
        setContentView(R.layout.activity_main);

        initUI();

        // Register the broadcast receiver for receiving the device connection's changes.
        IntentFilter filter = new IntentFilter();
        filter.addAction(DJISimulatorApplication.FLAG_CONNECTION_CHANGE);
        registerReceiver(mReceiver, filter);
    }

    /**
     * Checks if there is any missing permissions, and
     * requests runtime permission if needed.
     */
    private void checkAndRequestPermissions() {
        // Check for permissions
        for (String eachPermission : REQUIRED_PERMISSION_LIST) {
            if (ContextCompat.checkSelfPermission(this, eachPermission) != PackageManager.PERMISSION_GRANTED) {
                missingPermission.add(eachPermission);
            }
        }
        // Request for missing permissions
        if (!missingPermission.isEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    missingPermission.toArray(new String[missingPermission.size()]),
                    REQUEST_PERMISSION_CODE);
        }

    }

    /**
     * Result of runtime permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Check for granted permission and remove from missing list
        if (requestCode == REQUEST_PERMISSION_CODE) {
            for (int i = grantResults.length - 1; i >= 0; i--) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    missingPermission.remove(permissions[i]);
                }
            }
        }
        // If there is enough permission, we will start the registration
        if (missingPermission.isEmpty()) {
            startSDKRegistration();
        } else {
            showToast("Missing permissions!!!");
        }
    }

    private void startSDKRegistration() {
        if (isRegistrationInProgress.compareAndSet(false, true)) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    showToast( "registering, pls wait...");
                    DJISDKManager.getInstance().registerApp(getApplicationContext(), new DJISDKManager.SDKManagerCallback() {
                        @Override
                        public void onRegister(DJIError djiError) {
                            if (djiError == DJISDKError.REGISTRATION_SUCCESS) {
                                DJILog.e("App registration", DJISDKError.REGISTRATION_SUCCESS.getDescription());
                                DJISDKManager.getInstance().startConnectionToProduct();
                                showToast("Register Success");
                            } else {
                                showToast( "Register sdk fails, check network is available");
                            }
                            Log.v(TAG, djiError.getDescription());
                        }

                        @Override
                        public void onProductChange(BaseProduct oldProduct, BaseProduct newProduct) {
                            Log.d(TAG, String.format("onProductChanged oldProduct:%s, newProduct:%s", oldProduct, newProduct));
                        }
                    });
                }
            });
        }
    }

    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            updateTitleBar();
        }
    };

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateTitleBar() {
        if(mConnectStatusTextView == null) return;
        boolean ret = false;
        BaseProduct product = DJISimulatorApplication.getProductInstance();
        if (product != null) {
            if(product.isConnected()) {
                //The product is connected
                mConnectStatusTextView.setText(DJISimulatorApplication.getProductInstance().getModel() + " Connected");
                ret = true;
            } else {
                if(product instanceof Aircraft) {
                    Aircraft aircraft = (Aircraft)product;
                    if(aircraft.getRemoteController() != null && aircraft.getRemoteController().isConnected()) {
                        // The product is not connected, but the remote controller is connected
                        mConnectStatusTextView.setText("only RC Connected");
                        ret = true;
                    }
                }
            }
        }

        if(!ret) {
            // The product or the remote controller are not connected.
            mConnectStatusTextView.setText("Disconnected");
        }
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
        updateTitleBar();
        initFlightController();
        loginAccount();

    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    public void onReturn(View view){
        Log.e(TAG, "onReturn");
        this.finish();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        unregisterReceiver(mReceiver);
        if (null != mSendVirtualStickDataTimer) {
            mSendVirtualStickDataTask.cancel();
            mSendVirtualStickDataTask = null;
            mSendVirtualStickDataTimer.cancel();
            mSendVirtualStickDataTimer.purge();
            mSendVirtualStickDataTimer = null;
        }
        super.onDestroy();
    }

    private void loginAccount(){

        UserAccountManager.getInstance().logIntoDJIUserAccount(this,
                new CommonCallbacks.CompletionCallbackWith<UserAccountState>() {
                    @Override
                    public void onSuccess(final UserAccountState userAccountState) {
                        Log.e(TAG, "Login Success");
                    }
                    @Override
                    public void onFailure(DJIError error) {
                        showToast("Login Error:"
                                + error.getDescription());
                    }
                });
    }

    private void initFlightController() {

        Aircraft aircraft = DJISimulatorApplication.getAircraftInstance();
        if (aircraft == null || !aircraft.isConnected()) {
            showToast("Disconnected");
            mFlightController = null;
            return;
        } else {
            mFlightController = aircraft.getFlightController();
            mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
            mFlightController.setYawControlMode(YawControlMode.ANGLE);
            mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
            mFlightController.setRollPitchCoordinateSystem(FlightCoordinateSystem.BODY);
//            mFlightController.setMaxFlightRadiusLimitationEnabled(true,
//                    new CommonCallbacks.CompletionCallback() {
//                        @Override
//                        public void onResult(DJIError djiError) {
//                            if (djiError != null) {
//                                showToast(djiError.getDescription());
//                            } else {
//                                showToast("Set Max  Flight Radius On");
//                            }
//                        }
//                    });
            mFlightController.setMaxFlightRadius(200,
                    new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                showToast(djiError.getDescription());
                            }
                        }
                    });
            mFlightController.getMaxFlightRadius(new CommonCallbacks.CompletionCallbackWith<Integer>() {
                        @Override
                        public void onFailure(DJIError djiError) {
//                            if (djiError != null) {
//                                showToast(djiError.getDescription());
//                            } else {
//                                showToast("Get Max  Flight Radius");
//                            }
                        }
                    @Override
                    public void onSuccess(Integer t) {
//                        String radius = String.format("%.2d", t);
//                       showToast("Actually set Max  Flight Radius to" + Integer.toString(t));
                    }
            });
            mFlightController.getFlightAssistant().setCollisionAvoidanceEnabled(false,
                    new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                showToast(djiError.getDescription());
                            } else {
//                                showToast("Disabled Collision Avoidance");
                            }
                        }
                    });
            mFlightController.getFlightAssistant().setActiveObstacleAvoidanceEnabled(false,
                    new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                showToast(djiError.getDescription());
                            } else {
//                                showToast("Disabled Obstacle Avoidance");
                            }
                        }
                    });
            showToast("Initalised flight controller");
//            mFlightController.getSimulator().setStateCallback(new SimulatorState.Callback() {
//                @Override
//                public void onUpdate(final SimulatorState stateData) {
//                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            String yaw = String.format("%.2f", stateData.getYaw());
//                            String pitch = String.format("%.2f", stateData.getPitch());
//                            String roll = String.format("%.2f", stateData.getRoll());
//                            String positionX = String.format("%.2f", stateData.getPositionX());
//                            String positionY = String.format("%.2f", stateData.getPositionY());
//                            String positionZ = String.format("%.2f", stateData.getPositionZ());
//
//                            mTextView.setText("Yaw : " + yaw + ", Pitch : " + pitch + ", Roll : " + roll + "\n" + ", PosX : " + positionX +
//                                    ", PosY : " + positionY +
//                                    ", PosZ : " + positionZ);
//                            String latitudeX = String.format("%.8f", mFlightController.getState().getAircraftLocation().getLatitude());
//                            String longtitudeX = String.format("%.8f", mFlightController.getState().getAircraftLocation().getLongitude());
//                            mLocTextView.setText("Lat: " + latitudeX + ", Lon: " + longtitudeX);
//                            mTangE.setText(String.format("tang: %.4f %.4f \n norm: %.4f %.4f", e_tang, de_tang, e_norm, de_norm));
//                            updateCurrentInputTextView();
//
//                        }
//                    });
//                }
//            });
        }
    }

    private void initUI() {

        mBtnEnableVirtualStick = (Button) findViewById(R.id.btn_enable_virtual_stick);
        mBtnDisableVirtualStick = (Button) findViewById(R.id.btn_disable_virtual_stick);
        mBtnTakeOff = (Button) findViewById(R.id.btn_take_off);
        mBtnLand = (Button) findViewById(R.id.btn_land);
        mBtnSimulator = (ToggleButton) findViewById(R.id.btn_start_simulator);
        mTextView = (TextView) findViewById(R.id.textview_simulator);
        mLocTextView = (TextView) findViewById(R.id.textView_currentLocation);
        mTangE = (TextView) findViewById(R.id.textView_tange);
        mConnectStatusTextView = (TextView) findViewById(R.id.ConnectStatusTextView);

        mBtnEnableVirtualStick.setOnClickListener(this);
        mBtnDisableVirtualStick.setOnClickListener(this);
        mBtnTakeOff.setOnClickListener(this);
        mBtnLand.setOnClickListener(this);

        mBtnSimulator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    mTextView.setVisibility(View.VISIBLE);

                    if (mFlightController != null) {

//                        mFlightController.getSimulator()
//                                .start(InitializationData.createInstance(new LocationCoordinate2D(0.0004, 0.0004), 10, 10),
//                                        new CommonCallbacks.CompletionCallback() {
//                                    @Override
//                                    public void onResult(DJIError djiError) {
//                                        if (djiError != null) {
//                                            showToast(djiError.getDescription());
//                                        }else
//                                        {
//                                            showToast("Start Simulator Success");
//                                        }
//                                    }
//                                });

                        mUpdateOutputTimer = new Timer();
                        mUpdateOutputTimerTask = new updateOutputTimerTask();
                        mUpdateOutputTimer.schedule(mUpdateOutputTimerTask, 0, 200);
//                          updateOutput();
                    }

                } else {

                    mTextView.setVisibility(View.INVISIBLE);

                    if (mFlightController != null) {
//                        mFlightController.getSimulator()
//                                .stop(new CommonCallbacks.CompletionCallback() {
//                                            @Override
//                                            public void onResult(DJIError djiError) {
//                                                if (djiError != null) {
//                                                    showToast(djiError.getDescription());
//                                                }else
//                                                {
//                                                    showToast("Stop Simulator Success");
//                                                }
//                                            }
//                                        }
//                                );
                        if (null != mUpdateOutputTimer) {
                            mUpdateOutputTimerTask.cancel();
                            mUpdateOutputTimerTask= null;
                            mUpdateOutputTimer.cancel();
                            mUpdateOutputTimer.purge();
                            mUpdateOutputTimer= null;
                        }
                    }
                }
            }
        });

    }

    private void updateCurrentInputTextView(){
        String yaw = String.format("%.2f", mYaw);
        String throttle = String.format("%.2f", mThrottle);
        String pitch = String.format("%.2f", mPitch);
        String roll = String.format("%.8f", mRoll);
        TextView outField = (TextView) findViewById(R.id.textView_currentState);
        outField.setText("Yaw : " + yaw + "deg/s, Throttle: " + throttle + "m/s, Pitch : " + pitch + "m/s, Roll : " + roll + "m/s");
    }

    @Override
    public void onClick(View v) {
        TextView field;
        switch (v.getId()) {
            case R.id.btn_enable_virtual_stick:
                if (mFlightController != null){

                    mFlightController.setVirtualStickModeEnabled(true, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null){
                                showToast(djiError.getDescription());
                            }else
                            {
                                showToast("Enable Virtual Stick Success");
                            }
                        }
                    });

                }
                break;

            case R.id.btn_disable_virtual_stick:

                if (mFlightController != null){
                    mFlightController.setVirtualStickModeEnabled(false, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                showToast(djiError.getDescription());
                            } else {
                                showToast("Disable Virtual Stick Success");
                            }
                        }
                    });
                }
                break;

            case R.id.btn_take_off:
                if (mFlightController != null){
                    mFlightController.startTakeoff(
                            new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    if (djiError != null) {
                                        showToast(djiError.getDescription());
                                    } else {
                                        showToast("Take off Success");
                                    }
                                }
                            }
                    );

                }

                break;

            case R.id.btn_land:
                if (mFlightController != null){

                    mFlightController.startLanding(
                            new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    if (djiError != null) {
                                        showToast(djiError.getDescription());
                                    } else {
                                        showToast("Start Landing");
                                    }
                                }
                            }
                    );

                }

                break;
            case R.id.btn_send_yaw:
                    field = (TextView) findViewById(R.id.input_yaw);
                    if(!field.getText().toString().isEmpty()){
                        mYaw = Float.valueOf(field.getText().toString());
                    }
                if (mFlightController != null){
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);// this is in milliseconds
                    }
                }

                break;
            case R.id.btn_send_throttle:
                    field = (TextView) findViewById(R.id.input_throttle);
                    if(!field.getText().toString().isEmpty()){
                        mThrottle = Float.valueOf(field.getText().toString());
                    }
                    if (mFlightController != null){
                        if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);// this is in milliseconds
                    }
                }

                break;
            case R.id.btn_send_pitch:
                    updateOutput();
                    field = (TextView) findViewById(R.id.input_pitch);
                    if(!field.getText().toString().isEmpty()){
                        mPitch = Float.valueOf(field.getText().toString());
                    }
                if (mFlightController != null){
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);// this is in milliseconds
                    }
                }

                break;
            case R.id.btn_send_roll:
                    field = (TextView) findViewById(R.id.input_roll);
                    if(!field.getText().toString().isEmpty()){
                        mRoll = Float.valueOf(field.getText().toString());
                    }
                if (mFlightController != null){
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);// this is in milliseconds
                    }
                }

                break;
            case R.id.btn_send_Kp:
                field = (TextView) findViewById(R.id.input_Kp);
                if(!field.getText().toString().isEmpty()) {
                    gKp = Double.valueOf(field.getText().toString());
                }

                break;
            case R.id.btn_send_Kd:
                field = (TextView) findViewById(R.id.input_Kd);
                if(!field.getText().toString().isEmpty()) {
                    gKd = Double.valueOf(field.getText().toString());
                }

                break;
            case R.id.btn_reset:
                reset();
                break;
            case R.id.btn_send_waypoint:
                reset();
                showToast("Pressed the waypoint button");
                field = (TextView) findViewById(R.id.input_latitudeX);
                if(!field.getText().toString().isEmpty()){
                    desiredWaypointX = mFlightController.getState().getAircraftLocation().getLatitude() + Double.valueOf(field.getText().toString())/LAT_UNIT;
                }
                else desiredWaypointX = 0.f;
                field = (TextView) findViewById(R.id.input_longitudeY);
                if(!field.getText().toString().isEmpty()){
                    desiredWaypointY = mFlightController.getState().getAircraftLocation().getLongitude() + Double.valueOf(field.getText().toString())/LON_UNIT;
                }
                else desiredWaypointY = 0.f;
                if (mFlightController != null) {
                    travelToWaypoint();
                }
                break;
            default:
                break;
        }
    }
    private void reset(){
        mYaw = 0.f;
        mThrottle = 0.f;
        mRoll = 0.f;
        mPitch = 0.f;
        updateCurrentInputTextView();
        killTravelWaypointTimers();
    }
    private void killTravelWaypointTimers(){
        if (null != mCheckDriftTimer) {
            mCheckDriftTimerTask.cancel();
            mCheckDriftTimerTask= null;
            mCheckDriftTimer.cancel();
            mCheckDriftTimer.purge();
            mCheckDriftTimer= null;
        }
        if (null != mCheckTangentDriftTimer) {
            mCheckTangentDriftTimerTask.cancel();
            mCheckTangentDriftTimerTask= null;
            mCheckTangentDriftTimer.cancel();
            mCheckTangentDriftTimer.purge();
            mCheckTangentDriftTimer= null;
        }
    }
    private void travelToWaypoint(){
        LocationCoordinate3D currentLoc = mFlightController.getState().getAircraftLocation();
//        mFlightController.setYawControlMode(YawControlMode.ANGLE);
        double desiredYaw = 180.0/3.14159*atan2(LON_UNIT*(desiredWaypointY - currentLoc.getLongitude()),LAT_UNIT*(desiredWaypointX - currentLoc.getLatitude()));
        if (desiredYaw > 180.0) desiredYaw = desiredYaw - 360.0;
//        showToast("current yaw" + String.format("%.2f",currentYaw) + "desired Yaw: " + desiredYaw);
        mYaw = (float) desiredYaw;
        currentLoc = mFlightController.getState().getAircraftLocation();
//        showToast(String.format("currentloc: %.8f, %.8f", currentLoc.getLatitude(), currentLoc.getLongitude()));
        double xx = LAT_UNIT*(desiredWaypointX - currentLoc.getLatitude());
        double yy = LON_UNIT*(desiredWaypointY - currentLoc.getLongitude());
        magTang = sqrt(pow(xx, 2) + pow(yy,2));
        tangX = xx/magTang;
        tangY = yy/magTang;
//        showToast("Tang: " + String.format("%.2f %.2f", tangX, tangY));
        normalX = tangY;
        normalY = -tangX;
       if (null == mSendVirtualStickDataTimer) {
                mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                mSendVirtualStickDataTimer = new Timer();
                mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);// this is in milliseconds
            }
       //normal control
        mCheckDriftTimer = new Timer();
        mCheckDriftTimerTask = new CheckDriftTimerTask();
        mCheckDriftTimer.schedule(mCheckDriftTimerTask, 3000, 100);
        //tangential control
        mCheckTangentDriftTimer = new Timer();
        mCheckTangentDriftTimerTask = new CheckTangentDriftTimerTask();
        mCheckTangentDriftTimer.schedule(mCheckTangentDriftTimerTask, 3000, 100);
//        showToast("made control timers");
    }
    class SendVirtualStickDataTask extends TimerTask {

        @Override
        public void run() {

            if (mFlightController != null) {
                mFlightController.sendVirtualStickFlightControlData(
                        new FlightControlData(
                                mPitch, mRoll, mYaw, mThrottle
                        ), new CommonCallbacks.CompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {

                            }
                        }
                );
            }
        }
    }
    class CheckDriftTimerTask extends TimerTask {
        private double Kp = gKp/100.0;
        private double Kd = -gKd/100.0;
        @Override
        public void run(){
            ie_norm = 0.0;
            LocationCoordinate3D currentLoc = mFlightController.getState().getAircraftLocation();
            e_norm = (desiredWaypointY - currentLoc.getLongitude())*LON_UNIT*normalY +(desiredWaypointX - currentLoc.getLatitude())*LAT_UNIT*normalX;
            de_norm = mFlightController.getState().getVelocityX()*normalX + mFlightController.getState().getVelocityY()*normalY;
            mPitch = mPitch - (float) (Kp* e_norm + Kd* de_norm);
            mPitch = max(-Limits.ROLL_PITCH_CONTROL_MAX_VELOCITY,mPitch);
            mPitch = min(Limits.ROLL_PITCH_CONTROL_MAX_VELOCITY,mPitch);
        }
    }
    class CheckTangentDriftTimerTask extends TimerTask {
        private double Kp = -gKp/100.0;
        private double Kd = gKd/100.0;
        private double vswitch = 2.0;
        private double mRollcruise = 9.0;
        private double mRollBrakeDist = 0.25*mRollcruise*mRollcruise;
        @Override
        public void run(){

//            ie_tang = 0.0;
            LocationCoordinate3D currentLoc = mFlightController.getState().getAircraftLocation();
            e_tang = (desiredWaypointY -  currentLoc.getLongitude())*LON_UNIT*tangY +(desiredWaypointX - currentLoc.getLatitude())*LAT_UNIT*tangX;
            de_tang = mFlightController.getState().getVelocityX()*tangX + mFlightController.getState().getVelocityY()*tangY; //returns the velocity (positive means we
            if (e_tang > mRollBrakeDist) {
                mRoll = (float) mRollcruise;
            }
            else if (de_tang > vswitch) {
                mRoll= 0.f;
            }
            else {//PD control
                mRoll = mRoll - (float) (Kp * e_tang + Kd * de_tang);
                mRoll = max((float) (-vswitch), mRoll);
                mRoll = min((float) vswitch, mRoll);
            }
        }

    }
    class updateOutputTimerTask extends TimerTask {
        @Override
        public void run(){
            updateOutput();
        }
    }
    void updateOutput(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
//                String latitudeX = String.format("%.8f", mFlightController.getState().getAircraftLocation().getLatitude());
//                String longtitudeX = String.format("%.8f", mFlightController.getState().getAircraftLocation().getLongitude());
                String temppitch = String.format("%.8f",mFlightController.getState().getAttitude().pitch);
//                mLocTextView.setText("Lat: " + latitudeX + ", Long: " + longtitudeX + "pitch: " + temppitch);
                String Kpstring = String.format("%.2f", gKp);
                String Kdstring = String.format("%.2f", gKd);
                mLocTextView.setText("pitch: " + temppitch + "gKp: " + Kpstring + "gKd: " + Kdstring);
                mTangE.setText(String.format("tang: %.4f %.4f \n norm: %.4f %.4f", e_tang, de_tang, e_norm, de_norm));
                updateCurrentInputTextView();

            }
        });
    }
}
