# DJI Waypoint Navigation

## Introduction

This application implements autonomous navigation a DJI drone to GPS coordinates communicated in real-time. The entire process is automated including landing, takeoff and the transition to full speed PID-controlled flight between waypoints.
The application is develeped from the Android-SimulatorDemo repository from which the remainder of this README is copied.

## Requirements

 - Android Studio 2.0+
 - Android System 4.1+
 - DJI Android SDK 4.5
 
